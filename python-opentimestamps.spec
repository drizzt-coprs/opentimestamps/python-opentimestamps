%global pypi_name opentimestamps

Name:           python-%{pypi_name}
Version:        0.4.5
Release:        %autorelease
Summary:        Python3 library for creating and verifying OpenTimestamps proofs

License:        LGPLv3+
URL:            https://github.com/opentimestamps/%{name}
Source0:        https://github.com/opentimestamps/%{name}/archive/%{name}-v%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python3-devel python3-setuptools
# This is really CheckRequires ...
# BuildRequires:  python3-GitPython python3-pysha3
# End of CheckRequires

%description
Python3 library for creating and verifying OpenTimestamps proofs.


%package -n python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}
%{?python_enable_dependency_generator}

%description -n python3-%{pypi_name}
Python3 library for creating and verifying OpenTimestamps proofs.

%prep
%autosetup -n %{name}-%{name}-v%{version} -p1

%build
%py3_build

%install
%py3_install

# FIXME
#%check
#%{__python3} setup.py test

# Note that there is no %%files section for the unversioned python module
%files -n python3-%{pypi_name}
%license LICENSE
%doc README.md
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py?.*.egg-info

%changelog
%autochangelog
